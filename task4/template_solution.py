# This serves as a template which will guide you through the implementation of this task.  It is advised
# to first read the whole template and get a sense of the overall structure of the code before trying to fill in any of the T0DO gaps
# First, we import necessary libraries:
import pandas as pd
import numpy as np
import torch
import torch.nn as nn

from torch.utils.data import DataLoader, TensorDataset

from sklearn.model_selection import train_test_split
from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.linear_model import Ridge
from sklearn.gaussian_process import GaussianProcessRegressor
from sklearn.gaussian_process.kernels import WhiteKernel, RBF, Matern, DotProduct, RationalQuadratic
from sklearn.model_selection import GridSearchCV

device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

def load_data():
    """
    This function loads the data from the csv files and returns it as numpy arrays.

    input: None
    
    output: x_pretrain: np.ndarray, the features of the pretraining set
            y_pretrain: np.ndarray, the labels of the pretraining set
            x_train: np.ndarray, the features of the training set
            y_train: np.ndarray, the labels of the training set
            x_test: np.ndarray, the features of the test set
    """
    x_pretrain = pd.read_csv("./pretrain_features.csv", index_col="Id").drop("smiles", axis=1).to_numpy()
    y_pretrain = pd.read_csv("./pretrain_labels.csv", index_col="Id").to_numpy().squeeze(-1)
    x_train = pd.read_csv("./train_features.csv", index_col="Id").drop("smiles", axis=1).to_numpy()
    y_train = pd.read_csv("./train_labels.csv", index_col="Id").to_numpy().squeeze(-1)
    x_test = pd.read_csv("./test_features.csv", index_col="Id").drop("smiles", axis=1)
    return x_pretrain, y_pretrain, x_train, y_train, x_test

class Net(nn.Module):
    """
    The model class, which defines our feature extractor used in pretraining.
    """
    def __init__(self):
        """
        The constructor of the model.
        """
        super().__init__()
        # T0DO: Define the architecture of the model. It should be able to be trained on pretraing data 
        # and then used to extract features from the training and test data.
        self.linear_embedding = nn.Sequential(
            # you start with shape [256, 1000]
            nn.Linear(1000, 250),
            # shape is [256, 250]
            nn.ReLU(), 
            nn.Linear(250, 128),
            # shape is [256, 128]
            nn.ReLU(),
        )

        self.encoder = nn.Sequential(
            # N 1, 128
            # convolution1d and 2d expects either 3d shape or 4d shape
            # in the 1d case a 3d shape is expected
            # [batch size, CHANNELS, data shape] 
            # Convolution2d expects [batch size, CHANNELS, shape_x == rows, shape_y == cols]
            # expected shape [256, 1, 128] BUT YOU PROVIDE [256, 128]
            # x = x.reshape(x.shape[0], 1, x.shape[1])

            nn.Conv1d(in_channels=1, out_channels=8, kernel_size=32, stride=2, padding=1), # N 8, 50
            nn.ReLU(),
            nn.Conv1d(8, 16, 7, stride=2, padding=1), # N 16, 24
            nn.ReLU(),
            nn.Conv1d(16, 32, 3, stride=2, padding=1), # N 32, 12
            nn.ReLU(),
            nn.Conv1d(32, 64, 12), # N 64, 1
        )

        self.decoder = nn.Sequential(
            # N 64, 1
            nn.ConvTranspose1d(64, 32, 12), # N 32, 12
            nn.ReLU(),
            nn.ConvTranspose1d(32, 16, 3, stride=2, padding=1), # N 16, 24
            nn.ReLU(),
            nn.ConvTranspose1d(16, 8, 7, stride=2, padding=1),# N 8, 50
            nn.ReLU(),
            nn.ConvTranspose1d(8, 1, 32, stride=2, padding=0), # N 1, 128
            nn.ReLU(),
        )

        # your shape after the decoder is [256, 1, 128]
        # now you need to reshape again
        self.linear_decode = nn.Sequential(
            nn.Linear(128, 250), # N 1, 250
            nn.ReLU(),
            nn.Linear(250, 1000), # 1, 1000
            nn.Sigmoid(),
        )


    def forward(self, x):
        """
        The forward pass of the model.

        input: x: torch.Tensor, the input to the model

        output: x: torch.Tensor, the output of the model
        """
        # T0DO: Implement the forward pass of the model, in accordance with the architecture 
        # defined in the constructor.

        # you start with [256, 1000]
        x = self.linear_embedding(x)
        # here you are [256, 128]
        x = x.reshape(x.shape[0], 1, x.shape[1])
        # here the shape is [256, 1, 128]
        
        encoded = self.encoder(x)
        

        # start with the shape of [256, 64, 1]
        decoded = self.decoder(encoded)
        # end with the shape of [256, 1, 128]
        
        decoded = decoded.squeeze()
        
        decoded = self.linear_decode(decoded)

        return decoded
    
def make_feature_extractor(x, y, batch_size=256, eval_size=1000):
    """
    This function trains the feature extractor on the pretraining data and returns a function which
    can be used to extract features from the training and test data.

    input: x: np.ndarray, the features of the pretraining set
              y: np.ndarray, the labels of the pretraining set
                batch_size: int, the batch size used for training
                eval_size: int, the size of the validation set
            
    output: make_features: function, a function which can be used to extract features from the training and test data
    """
    # Pretraining data loading
    in_features = x.shape[-1]
    x_tr, x_val, y_tr, y_val = train_test_split(x, y, test_size=eval_size, random_state=0, shuffle=True)
    x_tr, x_val = torch.tensor(x_tr, dtype=torch.float), torch.tensor(x_val, dtype=torch.float)
    y_tr, y_val = torch.tensor(y_tr, dtype=torch.float), torch.tensor(y_val, dtype=torch.float)

    # model declaration
    model = Net()
    model.train()
    
    # T0DO: Implement the training loop. The model should be trained on the pretraining data. Use validation set 
    # to monitor the loss.
    model.to(device)
    n_epochs = 100
    loss_fn = nn.MSELoss()
    val_loss_prev = 1
    counter = 0
    optimizer = torch.optim.Adam(model.parameters(), lr=0.001, weight_decay=1e-5)
    #optimizer = torch.optim.SGD(model.parameters(), lr=0.01)

    dataset = TensorDataset(x_tr, y_tr)
    train_loader = DataLoader(dataset=dataset, batch_size=batch_size,shuffle= True, pin_memory=True, num_workers=4)

    val_dataset = TensorDataset(x_val, y_val)
    val_loader = DataLoader(dataset=val_dataset, batch_size=batch_size,shuffle= True, pin_memory=True, num_workers=4)

    for epoch in range(n_epochs):
        model.train(True)
        for batch, (X, y) in enumerate(train_loader):
            pred = model(X)
            loss = loss_fn(pred, X) #compute MSE between pred and input, thats the whole point of autoencoders!

            optimizer.zero_grad()
            loss.backward()
            optimizer.step()
        
        print("Epoch: ", epoch + 1, "Training-Loss: ", loss.item())

        #compute validation-error
        model.train(False)
        for batch, (X, y) in enumerate(val_loader):
            val_pred = model(X)
            val_loss = loss_fn(val_pred, X)
        
        print("Validation-Loss: ", val_loss.item())
        print("val/train-ratio: ", val_loss.item()/loss.item(), "\n") #if this ratio differs much from 1 we might overfit
        if(counter > 0 and  val_loss.item() > val_loss_prev):
            break

        if (val_loss.item() > val_loss_prev):
            counter +=1

        val_loss_prev = val_loss.item()
    


    def make_features(x):
        """
        This function extracts features from the training and test data, used in the actual pipeline 
        after the pretraining.

        input: x: np.ndarray, the features of the training or test set

        output: features: np.ndarray, the features extracted from the training or test set, propagated
        further in the pipeline
        """
        model.eval()
        # TODO: Implement the feature extraction, a part of a pretrained model used later in the pipeline.
        x_linear = model.linear_embedding(x)
        x_shaped = x_linear.reshape(x_linear.shape[0], 1, x_linear.shape[1])
        encoded = model.encoder(x_shaped)
        squeezed = encoded.squeeze()
        return squeezed #figure out how to inherit the encoder of our net

    return make_features

def make_pretraining_class(feature_extractors):
    """
    The wrapper function which makes pretraining API compatible with sklearn pipeline
    
    input: feature_extractors: dict, a dictionary of feature extractors

    output: PretrainedFeatures: class, a class which implements sklearn API
    """

    class PretrainedFeatures(BaseEstimator, TransformerMixin): #pretrainedfeaturesclass inherits baseestimator, transformermixin classes
        """
        The wrapper class for Pretraining pipeline.
        """
        def __init__(self, *, feature_extractor=None, mode=None):
            self.feature_extractor = feature_extractor
            self.mode = mode

        def fit(self, X=None, y=None):
            return self

        def transform(self, X):
            assert self.feature_extractor is not None
            X_new = feature_extractors[self.feature_extractor](X)
            return X_new
        
    return PretrainedFeatures

def get_regression_model():
    """
    This function returns the regression model used in the pipeline.

    input: None

    output: model: sklearn compatible model, the regression model
    """
    # T0DO: Implement the regression model. It should be able to be trained on the features extracted
    # by the feature extractor.
    #model = Ridge(alpha=1)
    gpr = GaussianProcessRegressor(random_state=42, n_restarts_optimizer=5)

    params = [RBF(l)*Matern(nu=0.5) + WhiteKernel(3) for l in np.linspace(3, 5, 4)]
    params = params + [RBF(2.5) + WhiteKernel(l) for l in np.linspace(2, 5, 6)]
    params = params + [Matern(nu=l) + WhiteKernel(2) for l in [0.5, 1,5, 2.5]]

    params_dict = {'kernel': params}

    model = GridSearchCV(gpr, param_grid=params_dict, scoring='r2', cv=4)

    return model

# Main function. You don't have to change this
if __name__ == '__main__':
    # Load data
    x_pretrain, y_pretrain, x_train, y_train, x_test = load_data()
    print("Data loaded!")
    print("values of x_pretrain range from", np.amin(x_pretrain), " to ", np.amax(x_pretrain))
    # Utilize pretraining data by creating feature extractor which extracts lumo energy 
    # features from available initial features
    feature_extractor =  make_feature_extractor(x_pretrain, y_pretrain)
    PretrainedFeatureClass = make_pretraining_class({"pretrain": feature_extractor})
    
    # regression model
    regression_model = get_regression_model()

    y_pred = np.zeros(x_test.shape[0])
    # T0DO: Implement the pipeline. It should contain feature extraction and regression. You can optionally
    # use other sklearn tools, such as StandardScaler, FunctionTransformer, etc.
    pretrained_feature_obj = PretrainedFeatureClass(feature_extractor="pretrain")
    x_train = pretrained_feature_obj.transform(torch.from_numpy(x_train).to(torch.float32))
    x_train = x_train.detach().numpy()
    regression_model.fit(x_train, y_train)
    print("\n regression-score: ", regression_model.score(x_train, y_train))
    x_test_trans = pretrained_feature_obj.transform(torch.from_numpy(x_test.to_numpy()).to(torch.float32))
    x_test_trans = x_test_trans.detach().numpy()
    y_pred = regression_model.predict(x_test_trans) #call predictor

    assert y_pred.shape == (x_test.shape[0],)
    y_pred = pd.DataFrame({"y": y_pred}, index=x_test.index)
    y_pred.to_csv("results.csv", index_label="Id")
    print("Predictions saved, all done!")