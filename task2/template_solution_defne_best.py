# This serves as a template which will guide you through the implementation of this task.  It is advised
# to first read the whole template and get a sense of the overall structure of the code before trying to fill in any of the gaps
# First, we import necessary libraries:
import numpy as np
import pandas as pd
from sklearn.experimental import enable_iterative_imputer
from sklearn.impute import IterativeImputer
from sklearn import datasets, svm
from sklearn.svm import SVR
from sklearn.model_selection import GridSearchCV
from sklearn.gaussian_process import GaussianProcessRegressor
from sklearn.gaussian_process.kernels import WhiteKernel, RBF, Matern, DotProduct, RationalQuadratic


def data_loading():
    """
    This function loads the training and test data, preprocesses it, removes the NaN values and interpolates the missing
    data using imputation

    Parameters
    ----------
    Returns
    ----------
    X_train: matrix of floats, training input with features
    y_train: array of floats, training output with labels
    X_test: matrix of floats: dim = (100, ?), test input with features
    """
    # Load training data
    train_df = pd.read_csv("train.csv")

    #print("Training data:")
    #print("Shape:", train_df.shape)
    #print(train_df.head(2))
    #print('\n')

    # Load test data
    test_df = pd.read_csv("test.csv")

    #print("Test data:")
    #print(test_df.shape)
    #print(test_df.head(2))

    # Dummy initialization of the X_train, X_test and y_train
    X_train = np.zeros_like(train_df.drop(['price_CHF'],axis=1))
    y_train = np.zeros_like(train_df['price_CHF'])
    X_test = np.zeros_like(test_df)

    # TODO: Perform data preprocessing, imputation and extract X_train, y_train and X_test
    # discard rows or columns with missing data
    # replace missing values with the corresponding mean or median
    # maybe check sklearn for preprocessing methods
    # https://scikit-learn.org/stable/modules/impute.html#impute

    season_map = {'spring': 1, 'summer': 2, 'autumn': 3, 'winter': 4}
    train_df = train_df.applymap(lambda s: season_map.get(s) if s in season_map else s)
    test_df = test_df.applymap(lambda s: season_map.get(s) if s in season_map else s)


    #imp_simple = SimpleImputer(missing_values=np.nan, strategy='mean') #can try strategy: mean, median, most_frequent
    #y_train = imp_simple.fit_transform(np.array(train_df['price_CHF']).reshape(-1, 1))

    imp_mult = IterativeImputer()
    train_temp = imp_mult.fit_transform(train_df) #apply IterativeImputer to all columns including price_chf
    y_train = train_temp[:,2]
    X_train = np.delete(train_temp, 2, 1)
    X_test = imp_mult.fit_transform(test_df)
    

    assert (X_train.shape[1] == X_test.shape[1]) and (X_train.shape[0] == y_train.shape[0]) and (X_test.shape[0] == 100), "Invalid data shape"
    return X_train, y_train, X_test

def modeling_and_prediction(X_train, y_train, X_test):
    """
    This function defines the model, fits training data and then does the prediction with the test data

    Parameters
    ----------
    X_train: matrix of floats, training input with 10 features
    y_train: array of floats, training output
    X_test: matrix of floats: dim = (100, ?), test input with 10 features

    Returns
    ----------
    y_test: array of floats: dim = (100,), predictions on test set
    """

    y_pred=np.zeros(X_test.shape[0])

    gpr = GaussianProcessRegressor(kernel=DotProduct(), random_state=42, n_restarts_optimizer=3)


    params = [RBF(l)*Matern(nu=0.5) + WhiteKernel(3) for l in np.linspace(3, 5, 4)]
    params = params + [RBF(2.5) + WhiteKernel(l) for l in np.linspace(2, 5, 6)]
    params = params + [Matern(nu=l) + WhiteKernel(2) for l in [0.5, 1,5, 2.5]]


    params_dict = {'kernel': params}

    clf_final = GridSearchCV(gpr, param_grid=params_dict, scoring='r2', cv=4)
    clf_final.fit(X_train, y_train)

    print("best params")
    print(clf_final.best_params_)
    print("best error")
    print(clf_final.best_score_)

    #return y_pred
    y_pred = clf_final.predict(np.array(X_test))

    assert y_pred.shape == (100,), "Invalid data shape"
    return y_pred

# Main function. You don't have to change this
if __name__ == "__main__":
    # Data loading
    X_train, y_train, X_test = data_loading()
    # The function retrieving optimal LR parameters
    y_pred=modeling_and_prediction(X_train, y_train, X_test)
    # Save results in the required format
    dt = pd.DataFrame(y_pred)
    dt.columns = ['price_CHF']
    dt.to_csv('results.csv', index=False)
    print("\nResults file successfully generated!")
