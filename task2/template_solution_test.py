# This serves as a template which will guide you through the implementation of this task.  It is advised
# to first read the whole template and get a sense of the overall structure of the code before trying to fill in any of the gaps
# First, we import necessary libraries:
import numpy as np
import pandas as pd
from sklearn.experimental import enable_iterative_imputer
from sklearn.impute import IterativeImputer
from sklearn.impute import SimpleImputer
from sklearn.model_selection import train_test_split
from sklearn import datasets
from sklearn import svm
from sklearn.kernel_ridge import KernelRidge
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import KFold
from sklearn.model_selection import GridSearchCV
from sklearn.gaussian_process import GaussianProcessRegressor
from sklearn.gaussian_process.kernels import DotProduct, RBF, Matern, RationalQuadratic, WhiteKernel

def data_loading():
    """
    This function loads the training and test data, preprocesses it, removes the NaN values and interpolates the missing 
    data using imputation

    Parameters
    ----------
    Returns
    ----------
    X_train: matrix of floats, training input with features
    y_train: array of floats, training output with labels
    X_test: matrix of floats: dim = (100, ?), test input with features
    """
    # Load training data
    train_df = pd.read_csv("train.csv")
    print("Training data:")
    print("Shape:", train_df.shape)
    print(train_df.head(2))
    print('\n')
    
    # Load test data
    test_df = pd.read_csv("test.csv")

    print("Test data:")
    print(test_df.shape)
    print(test_df.head(2))

    # Dummy initialization of the X_train, X_test and y_train   
    X_train = np.zeros_like(train_df.drop(['price_CHF'],axis=1))
    y_train = np.zeros_like(train_df['price_CHF'])
    X_test = np.zeros_like(test_df)

    # TODO: Perform data preprocessing, imputation and extract X_train, y_train and X_test
    # discard rows or columns with missing data
    # replace missing values with the corresponding mean or median
    # maybe check sklearn for preprocessing methods
    # https://scikit-learn.org/stable/modules/impute.html#impute

    imp_simple = SimpleImputer()
    y_train = imp_simple.fit_transform(np.array(train_df['price_CHF']).reshape(-1,1))

    imp_mult = IterativeImputer()
    X_train[:, 1:] = imp_mult.fit_transform(train_df.drop(['price_CHF', 'season'],axis=1)) #drop seasons col
    X_test[:, 1:] = imp_mult.fit_transform(test_df.drop(['season'], axis=1))
    #add seasons col back to X_train & X_test
    X_train[:, 0] = train_df['season']
    X_test[:, 0] = test_df['season'] #drop seasons cols

    #check that only missing vals are changed

    assert (X_train.shape[1] == X_test.shape[1]) and (X_train.shape[0] == y_train.shape[0]) and (X_test.shape[0] == 100), "Invalid data shape"
    x_shape=X_train.shape
    stop = round(4*x_shape[0]/5)
    X_train_new = X_train[0:stop, :]
    X_test_new = X_train[stop:, :]
    y_train_new = y_train[0:stop]
    y_test_new = y_train[stop:]

    return X_train_new, y_train_new, X_test, X_test_new, y_test_new

def modeling_and_prediction(X_train, y_train, X_test, X_test_new, y_test_new):
    """
    This function defines the model, fits training data and then does the prediction with the test data 

    Parameters
    ----------
    X_train: matrix of floats, training input with 10 features
    y_train: array of floats, training output
    X_test: matrix of floats: dim = (100, ?), test input with 10 features

    Returns
    ----------
    y_test: array of floats: dim = (100,), predictions on test set
    """

    y_pred=np.zeros(X_test.shape[0])
    #TODO: Define the model and fit it using training data. Then, use test data to make predictions
    # choose right kernel (linear, RBF, polynomial, Matern, RationalQuadratic, ) for kernelized ridge regression (cross-validation)
    # https://scikit-learn.org/stable/modules/cross_validation.html
    # or try Gaussian processes(maximize evidence, bayesian model selection)


    kernel = RBF(0.1)  #change only the kernel to optimize this code, larger RBF parameter causes error
    gpr = GaussianProcessRegressor(kernel=kernel, optimizer="fmin_l_bfgs_b")
    clf = GridSearchCV(gpr, param_grid={'kernel': [RBF(length_scale=l)*Matern(nu=a) for l in np.linspace(0.425, 0.5, 4)  for a in np.linspace(0.05, 0.08, 4)]})
    clf.fit(X_train[:, 1:], y_train)
    
    print("Best model from GS1:")
    print(clf.best_params_)
    print("Best R2 error FROM GS1:")
    print(clf.best_score_)
    y_pred = clf.predict(X_test[:, 1:])



    # estimator = KernelRidge(alpha=1.0, kernel='rbf') #try different kernels: linear, poly, rbf, gaussian, sigmoid, laplacian and alphas

    # custom = KFold(n_splits=2)

    # X_train_2 = np.delete(X_train, 0, axis=1)
    # estimator.fit(X_train_2,y_train)

    # scores = cross_val_score(estimator, X_train_2, y_train, cv=custom) #get a cross-validation score before returning vary splits
    # print(print("%0.2f accuracy with a standard deviation of %0.2f" % (scores.mean(), scores.std())))

    #final evaluation y_pred = estimator.predict(X_test)
    #y_pred = estimator.predict(X_test)

    assert y_pred.shape == (100,), "Invalid data shape"
    return y_pred

# Main function. You don't have to change this
if __name__ == "__main__":
    # Data loading
    X_train, y_train, X_test, X_test_new, y_test_new = data_loading()
    # The function retrieving optimal LR parameters
    y_pred=modeling_and_prediction(X_train, y_train, X_test, X_test_new, y_test_new)
    # Save results in the required format
    dt = pd.DataFrame(y_pred) 
    dt.columns = ['price_CHF']
    dt.to_csv('results.csv', index=False)
    print("\nResults file successfully generated!")

